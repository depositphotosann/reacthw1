import PropTypes from 'prop-types';

const ModalFooter = ({ firstText, secondaryText, firstClick, secondaryClick }) => {
  return (
    <div className="modalFooter">
      {firstText && <button className='firstBtn' onClick={firstClick}>{firstText}</button>}
      {secondaryText && <button className='secondBtn'  onClick={secondaryClick}>{secondaryText}</button>}
    </div>
  );
};

ModalFooter.propTypes = {
  firstText: PropTypes.string.isRequired,
  secondaryText: PropTypes.string.isRequired,
  firstClick: PropTypes.func.isRequired,
  secondaryClick: PropTypes.func.isRequired,
};

export default ModalFooter;