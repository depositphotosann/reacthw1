import Modal from './Modal';
import ModalWrapper from './ModalWrapper';
import ModalHeader from './ModalHeader';
import ModalFooter from './ModalFooter';
import ModalClose from './ModalClose';
import ModalBody from './ModalBody';
import ModalBackground from './ModalBackground';
import PropTypes from 'prop-types';

const ModalImage = ({ isOpen, onClose }) => {
  if (!isOpen) return null;

  return (
    <>
    <Modal>
      <ModalWrapper>
        <ModalHeader>Product Delete!</ModalHeader>
        <ModalBody>
          <div className="img"></div>
          <p>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</p>
        </ModalBody>
        <ModalFooter firstText="NO, CANCEL" secondaryText="YES, DELETE" firstClick={onClose} />
        <ModalClose onClick={onClose} />
      </ModalWrapper>
    </Modal>
    <ModalBackground click={onClose} />
    </>
  );
};

ModalImage.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ModalImage;