import PropTypes from "prop-types";

const ModalBackground = ({ click }) => {
  return <div onClick={click} className="modalBackground"></div>;
};

ModalBackground.propTypes = {
  click: PropTypes.func.isRequired,
};

export default ModalBackground;